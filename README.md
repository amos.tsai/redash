# Redash

## 第一次執行
1. docker-compose run --rm server create_db  # 會刪除重新建立redash系統資料庫
2. docker-compose up

## 後續常態執行
1. docker-compose up



## 資料庫資料來源
[US Stores Sales](https://www.kaggle.com/datasets/dsfelix/us-stores-sales?resource=download)